/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.pdfprovider

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

private const val FILENAME = "test.pdf"

class MainMotor(application: Application) : AndroidViewModel(application) {
  private val _states = MutableLiveData<MainViewState>()
  val states: LiveData<MainViewState> = _states
  private val assets = application.assets
  private val dest = File(application.filesDir, FILENAME)

  init {
    if (dest.exists()) {
      _states.value = MainViewState.Content(dest)
    }
  }

  fun exportPdf() {
    _states.value = MainViewState.Loading

    viewModelScope.launch(Dispatchers.IO) {
      try {
        assets.open(FILENAME).use { pdf ->
          FileOutputStream(dest).use { pdf.copyTo(it) }
        }
        _states.postValue(MainViewState.Content(dest))
      } catch (e: IOException) {
        _states.postValue(MainViewState.Error(e))
      }
    }
  }
}
