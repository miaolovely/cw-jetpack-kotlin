/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.todo.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.commonsware.todo.MainDispatcherRule
import com.commonsware.todo.repo.ToDoModel
import com.commonsware.todo.repo.ToDoRepository
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SingleModelMotorTest {
  @get:Rule
  val instantTaskExecutorRule = InstantTaskExecutorRule()

  @get:Rule
  val mainDispatcherRule = MainDispatcherRule()

  private val testModel = ToDoModel("this is a test")
  private val repo: ToDoRepository = mock()
  private lateinit var underTest: SingleModelMotor

  @Before
  fun setUp() {
    whenever(repo.find(testModel.id)).doReturn(flowOf(testModel))

    underTest = SingleModelMotor(repo, testModel.id)
  }

  @Test
  fun `initial state`() {
    val observer = underTest.states.test()

    mainDispatcherRule.dispatcher.runCurrent()
    observer.awaitValue().assertValue { it.item == testModel }

    verify(repo).find(testModel.id)
  }

  @Test
  fun `actions pass through to repo`() {
    val replacement = testModel.copy("whatevs")

    underTest.save(replacement)
    mainDispatcherRule.dispatcher.runCurrent()

    runBlocking { verify(repo).save(replacement) }

    underTest.delete(replacement)
    mainDispatcherRule.dispatcher.runCurrent()

    runBlocking { verify(repo).delete(replacement) }
  }
}