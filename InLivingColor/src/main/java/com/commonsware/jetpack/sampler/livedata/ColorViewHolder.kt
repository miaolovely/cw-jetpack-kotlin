/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.livedata

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row.view.*

class ColorViewHolder(
  row: View,
  private val onItemClick: (Int) -> Unit
) : RecyclerView.ViewHolder(row) {
  private val swatch: View = row.swatch
  private val label: TextView = row.label
  private var color: Int = 0x7FFFFFFF

  init {
    row.setOnClickListener { onItemClick(color) }
  }

  fun bindTo(color: Int) {
    this.color = color
    label.text = label.context.getString(R.string.label_template, color)
    swatch.setBackgroundColor(color)
  }
}
